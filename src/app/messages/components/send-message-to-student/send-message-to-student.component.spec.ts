import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {FormsModule} from "@angular/forms";
import { TranslateModule} from "@ngx-translate/core";
import { MessagesService} from "../../services/messages.service";
import {DIALOG_BUTTONS, ErrorService, LoadingService, ModalService, ToastService} from '@universis/common';
import { SendMessageToStudentComponent } from './send-message-to-student.component';
import {RouterTestingModule} from "@angular/router/testing";
import {BsModalService, ModalModule} from "ngx-bootstrap";

describe('SendMessageToStudentComponent', () => {
  let component: SendMessageToStudentComponent;
  let fixture: ComponentFixture<SendMessageToStudentComponent>;
  const loadingSvc = jasmine.createSpyObj('LoadingService', ['showLoading', 'hideLoading']);
  const messageSvc = jasmine.createSpyObj('MessagesService', ['sendMessageToStudent']);
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        FormsModule,
        RouterTestingModule,
        ModalModule.forRoot()
      ],
      declarations: [ SendMessageToStudentComponent ],
      providers: [
        BsModalService,
        ToastService,
        {
          provide: MessagesService,
          useValue: messageSvc
        },
        {
          provide: LoadingService,
          useValue: loadingSvc
        },
        ModalService,
        ErrorService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendMessageToStudentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
