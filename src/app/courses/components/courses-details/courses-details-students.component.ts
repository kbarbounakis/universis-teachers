import {Component, OnInit, Input, ElementRef, ViewChild, TemplateRef, Output, EventEmitter} from '@angular/core';
import { CoursesService } from '../../services/courses.service';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute, Router} from '@angular/router';
import {DIALOG_BUTTONS, ErrorService, ModalService, ToastService} from '@universis/common';
import {ResponseError} from '@themost/client';
import {LoadingService} from '@universis/common';
import {MessagesService} from '../../../messages/services/messages.service';




@Component({
  selector: 'app-courses-details-students',
  templateUrl: './courses-details-students.component.html',
  styleUrls: ['./courses-details.component.scss']
})
export class CoursesDetailsStudentsComponent implements OnInit {
  public selectedClass: any;
  public exams: any;
  public courseClassStudents: any;
  public searchText = '';
  public selectedClassSection = null;
  selector = '.inner__content';
  public scrollValues: any;
  public take = 50;
  public skip = 0;
  public count: any;
  private selectedSection: any;
  @ViewChild('innerContainer') innerContainer: ElementRef;
  @ViewChild('templateMsg') msgTemplate: TemplateRef<any>;
  @Output() succesfulSend = new EventEmitter<boolean>();
  private modalRef: any;
  public messageModel = {
    body: null,
    subject: null
  };
  private courseClassId: any;
  private studentId: any;
  private studentFamilyName: any;
  private studentGivenName: any;
  private studentIdentifierId: any;


  constructor(private _context: AngularDataContext,
              private translate: TranslateService,
              private messagesService: MessagesService,
              private modal: ModalService,
              private toastService: ToastService,
              private coursesService: CoursesService,
              private errorService: ErrorService,
              private loadingService: LoadingService,
              private router: Router,
              private route: ActivatedRoute) {
  }
  ngOnInit() {
    this.loadingService.showLoading();
    this.route.parent.params.subscribe(routeParams => {
      this.coursesService.getCourseClass(routeParams.course, routeParams.year, routeParams.period).then(courseClass => {
        if (typeof courseClass === 'undefined') {
          return this.errorService.navigateToError(new ResponseError('Course class cannot be found or is inaccessible', 404));
        }
        this.initializeData();
        // set selected course class
        this.selectedClass = courseClass;
        // get course class students
        this.coursesService.getCourseClassStudents(courseClass.id, this.skip, this.take).getList().then( students => {
          // hide loading
          this.loadingService.hideLoading();
          this.loadData(students);
        });
      }).catch(err => {
        // hide loading
        this.loadingService.hideLoading();
        return this.errorService.navigateToError(err);
      });
    });
  }
  getStudentsdata() {
    this.loadingService.showLoading();
    if (this.searchText.length === 0) {
      if (this.selectedClassSection == null) {
        this.coursesService.getCourseClassStudents(this.selectedClass.id, this.skip, this.take).getList().then(students => {
          this.count = students.total;
          this.courseClassStudents = students.value;
          for (let i = 0; i < this.courseClassStudents.length; i++) {
            this.scrollValues.push(this.courseClassStudents[i]);
          }
          this.loadingService.hideLoading();
        }); }
      this.coursesService.getCourseClassStudents(this.selectedClass.id, this.skip, this.take).where('section')
        .equal(this.selectedClassSection).getList().then(students => {
          this.count = students.total;
        this.courseClassStudents = students.value;
        for (let i = 0; i < this.courseClassStudents.length; i++) {
          this.scrollValues.push(this.courseClassStudents[i]);
        }
        this.loadingService.hideLoading();
      }); } else {
      this.coursesService.searchCourseClassStudents(this.selectedClass.id, this.searchText, this.skip, this.take).getList()
        .then(students => {
          this.count = students.total;
          this.courseClassStudents = students.value;
          for (let i = 0; i < this.courseClassStudents.length; i++) {
            this.scrollValues.push(this.courseClassStudents[i]);
          }
          this.loadingService.hideLoading();
        });
    }
  }

  exportStudentList(courseClass) {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    let fileURL;
    if (this.selectedClassSection || this.searchText ) {
      if (this.selectedClassSection && this.searchText) {
        fileURL = this._context.getService()
          .resolve(`instructors/me/classes/${courseClass.id}
        /students/export?$filter=section eq ${this.selectedClassSection}
        and (indexof(student/person/givenName,'${this.searchText}')ge 0
        or indexof(student/person/familyName,'${this.searchText}')ge 0
        or indexof(student/studentIdentifier,'${this.searchText}')ge 0)`);
      } else if (this.selectedClassSection && !this.searchText) {
        // append class section query
        fileURL = this._context.getService()
          .resolve(`instructors/me/classes/${courseClass.id}/students/export?$filter=section eq ${this.selectedClassSection}`);
      } else if (this.searchText && !this.selectedClassSection) {
        // append class section query
        fileURL = this._context.getService()
          .resolve(`instructors/me/classes/${courseClass.id}
        /students/export?$filter= indexof(student/person/givenName,'${this.searchText}')ge 0
        or indexof(student/person/familyName,'${this.searchText}')ge 0
        or indexof(student/studentIdentifier,'${this.searchText}')ge 0)`);
      }
    }    else {
      fileURL = this._context.getService().resolve(`instructors/me/classes/${courseClass.id}/students/export`);
    }
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {

      return response.blob();
    })
      .then(blob => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        const filtered = this.translate.instant('CoursesLocal.filtered');
        a.href = objectUrl;
        if (this.selectedClassSection || this.searchText ) {
          if (this.selectedClassSection && this.searchText) {
        a.download = `${courseClass.title}-${courseClass.year.id}-${courseClass.period.name}-${this.selectedClassSection}-${filtered}.xlsx`;
          } else if (this.selectedClassSection && !this.searchText) {
            a.download = `${courseClass.title}-${courseClass.year.id}-${courseClass.period.name}-${this.selectedClassSection}.xlsx`;
          } else if (this.searchText && !this.selectedClassSection) {
            a.download = `${courseClass.title}-${courseClass.year.id}-${courseClass.period.name}-${filtered}.xlsx`;
          }
        } else {
          a.download = `${courseClass.title}-${courseClass.year.id}-${courseClass.period.name}.xlsx`;
        }
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove(); // remove the element
      });

  }

  onSearchTextKeyDown($event: any) {
    if ($event.keyCode === 13) {
      this.initializeData();
      this.loadingService.showLoading();
      // get search client data queryable
      const query = this.coursesService.searchCourseClassStudents(this.selectedClass.id, this.searchText, this.skip, this.take);
      if (this.selectedClassSection) {
        // append query
        query.and('section').equal(this.selectedClassSection);
      }        return query.getList().then((res) => {
        this.loadingService.hideLoading();
        this.loadData(res);
      }).catch( err => {
        // handle error (show in-place error)
        this.loadingService.hideLoading();
      });
    }
  }


  onSearchTextKeyUp($event: any) {

    if ($event.target && $event.target.value.length === 0) {
      this.loadingService.showLoading();
      this.initializeData();
      // get course class student client data queryable
      const query = this.coursesService.getCourseClassStudents(this.selectedClass.id, this.skip, this.take);
      // check if a selected class section exists
      if (this.selectedClassSection) {
        // append query
        query.and('section').equal(this.selectedClassSection);
      }
      // and finally take all items
      return query.getList().then( students => {
        this.loadingService.hideLoading();
        this.loadData(students);
      });
    }
  }

  selectChangeHandler (selected: any) {
    // show loading
    this.loadingService.showLoading();
    this.searchText = '';
    this.initializeData();
    this.selectedSection = selected;
    if (selected === 'null' || selected === null)   {
      return this.coursesService.getCourseClassStudents(this.selectedClass.id, this.skip, this.take).getList().then( res => {
        // hide loading
        this.loadingService.hideLoading();
        this.loadData(res);
      }).catch(err => {
        return this.errorService.navigateToError(err);
      });
    } else      {
      return this.coursesService.getCourseClassStudents(this.selectedClass.id, this.skip, this.take).where('section').equal(selected)
        .getList().then(res => {
          // hide loading
          this.loadingService.hideLoading();
          this.loadData(res);
        }).catch(err => {
          return this.errorService.navigateToError(err);
        });
    }
  }
  onScroll() {
    this.skip += this.take;
    this.getStudentsdata();
  }
  loadData(studentData) {
    studentData = studentData || {
      value: [],
      total: 0
    };
    this.count = studentData.total;
    this.courseClassStudents = studentData.value;
    this.scrollValues = this.courseClassStudents;
  }
  initializeData() {
    this.skip = 0;
    $(this.innerContainer.nativeElement).scrollTop(0);
  }
  openMsg (courseClassStudent) {
    this.openModal(this.msgTemplate);
    this.courseClassId = courseClassStudent.courseClass.id;
    this.studentId = courseClassStudent.student.id;
    this.studentFamilyName = courseClassStudent.student.familyName;
    this.studentGivenName = courseClassStudent.student.givenName;
    this.studentIdentifierId = courseClassStudent.student.studentIdentifier;
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modal.openModal(template, 'modal-lg');
  }

  closeModal() {
    // Reload the page. A more specific function may be implemented later
    this.ngOnInit();
    this.modalRef.hide();
  }
  sendStudentMessage() {
          this.loadingService.showLoading();
          this.messagesService.sendMessageToStudent(this.courseClassId, this.studentId, this.messageModel)
            .subscribe(msgSend => {
              // complete the action
              this.initData();

              this.loadingService.hideLoading();
              this.toastService.show( this.translate.instant('Modals.SuccessfulSendMessageTitle'),
                this.translate.instant('Modals.SuccessfulSendMessageMessage'));
              let container = document.body.getElementsByClassName('universis-toast-container')[0];
              if (container != null) {
                container.classList.add('toast-success');
              }
              this.messageSent(true);
            }, (err) => {
              this.loadingService.hideLoading();
              this.toastService.show( this.translate.instant('Modals.FailedSendMessageTitle'),
                this.translate.instant('Modals.FailedSendMessageMessage'));
              let container = document.body.getElementsByClassName('universis-toast-container')[0];
              if (container != null) {
                container.classList.add('toast-error');
              }
              this.messageSent(false);
            });
  }
  initData() {
    this.messageModel.body = null;
    this.messageModel.subject = null;
  }
  messageSent(succesful: boolean) {
    this.succesfulSend.emit(succesful);
  }
}
